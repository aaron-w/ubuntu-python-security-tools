#!/usr/bin/env python3
# Copyright Aaron Whitehouse
# See LICENSE.txt for licence details

from debian.debian_support import Version
import requests
import json
import os
import time


def download_notices_data(output_file, minimised_cache_file, release=None):
    # URL of the API endpoint
    url = "https://ubuntu.com/security/notices.json"

    limit = 10  # Max is 100, but I have seen issues at higher numbers
    total_results = 0
    notices_list = []
    retrieved_so_far = 0

    # Retrieve the total number of results available
    parameters = {'limit': limit, 'offset': 0}
    if release:
        parameters.update({'release': release})

    print(f"response = requests.get({url}, params={parameters})")
    response = requests.get(url, params=parameters)
    if response.status_code == 200:
        results = response.json()
        notices_list.extend(results.get('notices'))
        total_results = results.get('total_results')
        total_results = int(total_results) if total_results else 0
        retrieved_so_far = min(total_results, limit)

        # Retrieve remaining results
        while retrieved_so_far < total_results:
            print(f"Requesting USN records {retrieved_so_far + 1} to {retrieved_so_far + limit} (of {total_results})")
            parameters = {'limit': limit, 'offset': retrieved_so_far}
            if release:
                parameters.update({'release': release})
            response = requests.get(url, params=parameters)
            retrieved_so_far = min(retrieved_so_far + limit, total_results)

            if response.status_code == 200:
                results = response.json()
                notices_list.extend(results.get('notices'))
            else:
                print(f"Error: Failed to download data. Status code: {response.status_code}")
                return response.status_code

        # Reconstruct the original format
        reconstructed_json = {'notices': notices_list, 'limit': total_results, 'offset': 0,
                              'total_results': total_results}

        os.makedirs(os.path.dirname(output_file), exist_ok=True)

        with open(output_file, 'w') as file:
            json.dump(reconstructed_json, file, indent=4)

        print(f"Data downloaded and saved to {output_file} successfully.")
        print(f"Total results retrieved: {retrieved_so_far}")
        print("Creating minimal version of USN data to make subsequent runs faster.")

        minimal_notices = minimise_notices(notices_list)
        # Reconstruct the original format with the minimised JSON
        reconstructed_json = {'notices': minimal_notices, 'limit': total_results, 'offset': 0,
                              'total_results': total_results}
        with open(minimised_cache_file, 'w') as file:
            json.dump(reconstructed_json, file, separators=(',', ':'))
    else:
        print(f"Error: Failed to retrieve total results. Status code: {response.status_code}")
        return response.status_code


def minimise_notices(notices_list):
    """Minimises the notices section of the USN JSON to just those fields that
    are required"""
    minimal_notices = []
    for notice in notices_list:
        minimised_notice = {}
        for item_to_keep in ["id", "title", "cves_ids"]:
            # We want to keep these items of the JSON in full
            minimised_notice[item_to_keep] = notice.get(item_to_keep)
        if notice.get("cves"):
            minimised_notice["cves"] = []
            for cve in notice.get("cves"):
                # We only want to keep some of the CVE detail
                minimised_cve_entry = {}
                for item_to_keep in ["id", "priority", "cvss3"]:
                    minimised_cve_entry[item_to_keep] = \
                        cve.get(item_to_keep)
                minimised_notice["cves"].append(minimised_cve_entry)
        if notice.get("release_packages"):
            minimised_notice["release_packages"] = {}
            for release in notice.get("release_packages"):
                minimised_notice["release_packages"][release] = []
                for release_package in notice.get("release_packages").get(release):
                    minimised_release_package = {}
                    for item_to_keep in ["is_source", "name", "version"]:
                        minimised_release_package[item_to_keep] = release_package.get(item_to_keep)
                    minimised_notice["release_packages"][release].append(minimised_release_package)
        minimal_notices.append(minimised_notice)
    return minimal_notices

class PackagesUpdatesFromJSONFile:
    """Contains operations for working with the output of pro api u.pro.packages.updates.v1
    https://github.com/canonical/ubuntu-pro-client/blob/main/docs/references/api.md#upropackagesupdatesv1
    saved to a file."""
    def __init__(self, json_file):
        with open(json_file) as file:
            self.updates_json = json.load(file)

        self.updates = self.updates_json.get("data").get("attributes").get("updates")


class ManifestFromJSONFile:
    """Contains operations for working with the output of pro api u.security.package_manifest.v1
    https://github.com/canonical/ubuntu-pro-client/blob/main/docs/references/api.md#usecuritypackage_manifestv1
    saved to a file."""
    def __init__(self, json_file):
        self.installed_deb_versions = {}

        with open(json_file) as file:
            self.manifest_data = json.load(file)

        manifest = self.manifest_data.get("data").get("attributes").get("manifest_data")

        self.deb_list = []
        self.snap_list = []

        if manifest:
            for line in manifest.splitlines():
                if line[:5] == "snap:":
                    split_entry = line[5:].split('\t')
                    package = split_entry[0]
                    tracking = split_entry[1]
                    revision = split_entry[2]

                    self.snap_list.append({'package': package, 'tracking': tracking, 'revision': revision})
                else:
                    split_entry = line.split('\t')
                    version = split_entry[1]
                    # print(line)
                    package_arch = split_entry[0].split(':')
                    if len(package_arch) == 2:
                        # line has both a package and an arch
                        package = package_arch[0]
                        arch = package_arch[1]
                    else:
                        # No arch
                        package = package_arch[0]
                        arch = ''

                    self.deb_list.append({'package': package, 'arch': arch, 'version': version})
                    self.installed_deb_versions[package] = version


class UsnData:
    """Contains data from the Ubuntu Security CVE API and relevant operations on that data."""

    def __init__(self, local_json_file=None):
        self.cve_detail = {}
        self.usn_detail = {}

        if not local_json_file:
            cache_file = os.path.join(".cache", "usn_data.json")
            minimised_cache_file = os.path.join(".cache", "usn_data_minimised.json")

            # If the cache does not exist or is more than a day old, download it again
            if os.path.exists(cache_file):
                cache_file_mtime = os.stat(cache_file).st_mtime
                current_time_seconds = time.time()
                cache_age = current_time_seconds - cache_file_mtime

                if cache_age > (60 * 60 * 24):
                    print("Cached JSON is too old, downloading again.")
                    download_notices_data(cache_file, minimised_cache_file)
            else:
                print("No local JSON file cached, downloading.")
                return_code = download_notices_data(cache_file, minimised_cache_file)
                if return_code != 200:
                    print("Warning, download failed. Will use old copy of JSON file if available.")

            if os.path.exists(minimised_cache_file):
                local_json_file = minimised_cache_file
            else:
                local_json_file = cache_file

        if os.path.exists(local_json_file):
            # Possible for it not to exist if no local file is defined and the download fails
            # in which case if we still have a (stale) local file, we may as well use that.
            with open(local_json_file) as file:
                self.usn_data = json.load(file)
        else:
            print("No local file is available.")

    def vulnerabilities_fixed_between_versions(self, release, package, installed_version, available_version):
        """Given the Ubuntu release, the relevant package and both the installed and available versions, this returns a
        list of the CVE IDs that were fixed between installed_version and available_version"""

        cves = []
        notices = []

        for notice in self.usn_data.get("notices"):
            if notice.get("release_packages") and release in notice.get("release_packages").keys():
                for release_package in notice.get("release_packages").get(release):
                    if release_package.get("name") == package and not release_package.get("is_source"):
                        # is_source check is required, for example because 'linux-azure' is 5.4 series for Focal
                        # as source, but currently 5.15 series as binary packages (the vice versa is available,
                        # but with different names).
                        fixed_version = Version(release_package.get("version"))
                        installed_version = Version(installed_version)
                        if installed_version < fixed_version:
                            # Current version is vulnerable to the CVE
                            available_version = Version(available_version)
                            if available_version >= fixed_version:
                                # Available update would fix the CVE.
                                notice_id = notice.get("id")
                                if notice_id and (notice_id not in notices):
                                    notices.append(notice_id)
                                    if notice_id not in self.usn_detail:
                                        # ToDo: This is a bit nasty, as could refer to CVE detail before it is saved
                                        usn_detail_to_save = {"cves_ids": notice.get("cves_ids"),
                                                              "title": notice.get("title")}
                                        self.usn_detail[notice_id] = usn_detail_to_save

                                for cve in notice.get("cves_ids"):
                                    if cve not in cves:
                                        cves.append(cve)

                                for cve_detail_entry in notice.get("cves"):
                                    if cve_detail_entry.get("id") not in self.cve_detail:
                                        # ToDo: This is a bit nasty, as could refer to CVE detail before it is saved
                                        self.cve_detail[cve_detail_entry.get("id")] = cve_detail_entry

        return {"cves_ids": cves, "notices_ids": notices}


class USNDataFromManifestAndUpdatesJson(UsnData):
    def __init__(self, manifest: ManifestFromJSONFile, updates: PackagesUpdatesFromJSONFile, release,
                 local_json_file=None):
        UsnData.__init__(self, local_json_file)

        self.usns_fixed_by_available_updates = {}
        self.fixed_usns = []
        for deb_update in updates.updates:
            package_name = deb_update.get("package")
            available_version = deb_update.get("version")
            if ":" in package_name:
                # This is likely because there is a package installed from another arch, e.g. i386 on AMD64
                # For now we will just ignore this, as security updates are normally consistent across arches
                package_name = package_name[:package_name.find(":")]

            fixed_vulnerabilities = \
                self.vulnerabilities_fixed_between_versions(release, package_name,
                                                            manifest.installed_deb_versions[package_name],
                                                            available_version)
            if not self.usns_fixed_by_available_updates.get(deb_update.get("package")) or \
                    (Version(self.usns_fixed_by_available_updates.get(deb_update.get(
                        "package")).get("available_version")) < Version(available_version)):
                self.usns_fixed_by_available_updates.update(
                    {deb_update.get("package"): {"installed_version": manifest.installed_deb_versions[package_name],
                                                 "available_version": available_version,
                                                 "provided_by": deb_update.get("provided_by"),
                                                 "fixed_usns": fixed_vulnerabilities.get("notices_ids")}})
            # print(deb_update.get("package"), fixed_vulnerabilities.get("notices_ids"))
            for fixed_usn in fixed_vulnerabilities.get("notices_ids"):
                if fixed_usn not in self.fixed_usns:
                    self.fixed_usns.append(fixed_usn)

    def usns_and_cves_in_oscap_results_html_format(self):
        """Returns USN and CVE data in a format that makes it easy to compare with the output of the middle column of
        oscap oval eval --report report.html com.ubuntu.$(lsb_release -cs).usn.oval.xml
        """
        results = []
        for usn in sorted(self.fixed_usns):
            vulnerabilities = [usn]
            vulnerabilities.extend(sorted(self.usn_detail[usn].get("cves_ids")))
            vulnerabilities = ["[" + s + "]" for s in vulnerabilities]
            title_usn = usn + " -- " + self.usn_detail.get(usn).get("title")
            results.append((vulnerabilities, title_usn))

        # print(results)
        return results

    def tabulate_table_with_update_and_vulnerability_details(self):
        """Returns a table (list of lists) with each row containing package name, installed version, available version,
        provided by and fixed vulnerabilities for each available update, suitable for use with tabulate"""

        table = []

        for update in self.usns_fixed_by_available_updates:
            update_details = self.usns_fixed_by_available_updates.get(update)
            vulnerability_details = ""
            for fixed_usn in update_details.get("fixed_usns"):
                if vulnerability_details == "":
                    # First entry
                    vulnerability_details = fixed_usn + " -- " + self.usn_detail.get(fixed_usn).get("title")
                else:
                    vulnerability_details += "\n" + fixed_usn + " -- " + self.usn_detail.get(fixed_usn).get("title")
                for cve in sorted(self.usn_detail[fixed_usn].get("cves_ids")):
                    cve_details = f"{cve}, Priority: {self.cve_detail.get(cve).get('priority')}, " \
                                  f"CVSS3: {self.cve_detail.get(cve).get('cvss3')}"
                    vulnerability_details += f"\n    {cve_details}"

            table.append(
                [update, update_details.get("installed_version"), update_details.get("available_version"),
                 update_details.get("provided_by"), vulnerability_details])
        return table


if __name__ == '__main__':
    import argparse
    import pathlib
    import tabulate

    parser = argparse.ArgumentParser()
    parser.add_argument("manifest", type=pathlib.Path,
                        help="Path to the output of 'pro api u.security.package_manifest.v1 > [file.json]'")
    parser.add_argument("update_info_json", type=pathlib.Path,
                        help="Path to the output of 'pro api u.pro.packages.updates.v1 > [updates.json]'")
    parser.add_argument("release", help="The name of the Ubuntu release, e.g. 'focal'")
    parser.add_argument("--local_USN_data", dest='local_json',
                        help="Use a local JSON file instead of downloading from https://ubuntu.com/security/api/docs")

    args = parser.parse_args()

    manifest = ManifestFromJSONFile(args.manifest)
    updates = PackagesUpdatesFromJSONFile(args.update_info_json)

    usns = USNDataFromManifestAndUpdatesJson(manifest=manifest, updates=updates,
                                             release=args.release, local_json_file=args.local_json)

    table_headers = ["Package Update Details", "Installed Version", "Available Version", "Provided By",
                     "Fixed Vunlerabilities"]

    table_to_print = usns.tabulate_table_with_update_and_vulnerability_details()

    print(tabulate.tabulate(table_to_print, table_headers, tablefmt="fancy_grid"))
