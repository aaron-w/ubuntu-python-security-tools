Canonical:0001-com-ubuntu-server-focal:20_04-lts-gen2:20.04.202303020
vs OVAL file of 19 May 2023

report.html generated in a fresh Canonical:0001-com-ubuntu-server-focal:20_04-lts-gen2:20.04.202303020 instance on Azure with the following instructions:
1) sudo apt update
2) sudo apt install ubuntu-advantage-tools
3) wget https://security-metadata.canonical.com/oval/com.ubuntu.$(lsb_release -cs).usn.oval.xml.bz2
4) bunzip2 com.ubuntu.$(lsb_release -cs).usn.oval.xml.bz2
5) sudo apt install libopenscap8
6) oscap oval eval --report report.html com.ubuntu.$(lsb_release -cs).usn.oval.xml

api_packages_updates.json is the output of
pro api u.pro.packages.updates.v1 as per
https://github.com/canonical/ubuntu-pro-client/blob/main/docs/references/api.md#upropackagesupdatesv1

api_packagemanifest.json is the output of 
pro api u.security.package_manifest.v1
https://github.com/canonical/ubuntu-pro-client/blob/main/docs/references/api.md#usecuritypackage_manifestv1

api_packages_summary.json is not used, but just in case is the output of
pro api u.pro.packages.summary.v1
https://github.com/canonical/ubuntu-pro-client/blob/main/docs/references/api.md#upropackagessummaryv1

usn_data.json is the downloaded USN data from:
https://ubuntu.com/security/notices.json
on 19 May 2023

This test shows that it is possible to reconstruct an equivalent analysis to the "patch" sections of an oscap scan using official Ubuntu OVAL data using a combination of the Notices endpoint of the Security API and the manifest from the instance.
