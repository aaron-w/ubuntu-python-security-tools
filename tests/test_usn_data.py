import json
import os
import unittest
from main import UsnData, USNDataFromManifestAndUpdatesJson, ManifestFromJSONFile, PackagesUpdatesFromJSONFile, \
    minimise_notices
import shutil
import zipfile


# class TestMinimise(unittest.TestCase):
#
#     def test_minimise(self):
#         test_data_dir = os.path.join('testdata', '20230206_vs_20230519')
#         temp_dir_for_json = os.path.join(test_data_dir, 'tmp_for_json_extraction')
#
#         with zipfile.ZipFile(os.path.join('testdata', '20230206_vs_20230519',
#                                           '20230519_focal_usn_data.zip'), 'r') as zip_ref:
#             zip_ref.extractall(temp_dir_for_json)
#
#         json_data = os.path.join(temp_dir_for_json, "20230519_focal_usn_data.json")
#
#         with open(json_data) as file:
#             json_to_minimise = json.load(file)
#             print(minimise_notices(json_to_minimise.get("notices")))


class TestUSNs(unittest.TestCase):

    def setUp(self):
        self.usn_data = UsnData(os.path.join('testdata', '20230517_notices_10_250offset_focal.json'))

    def test_vulnerabilities_between_libtiff_dev_versions(self):
        release = "focal"
        package = "libtiff-dev"
        # Fixed in "4.1.0+git191117-2ubuntu0.20.04.7"
        installed_version = "4.1.0+git191117-2ubuntu0.20.04.4"
        available_version = "4.1.0+git191117-2ubuntu0.20.04.8"
        expected_cves = ["CVE-2022-3970"]

        actual_vulnerabilities = self.usn_data.vulnerabilities_fixed_between_versions(release, package,
                                                                                      installed_version,
                                                                                      available_version)

        actual_cves = actual_vulnerabilities.get("cves_ids")
        self.assertEqual(expected_cves, actual_cves)

        self.assertEqual("medium", self.usn_data.cve_detail.get("CVE-2022-3970").get("priority"))

        expected_usns = ["USN-5743-2"]
        actual_usns = actual_vulnerabilities.get("notices_ids")

        self.assertEqual(expected_usns, actual_usns)


class TestComparisonWithOscap(unittest.TestCase):

    def setUp(self):
        test_data_dir = os.path.join('testdata', '20230206_vs_20230519')
        self.temp_dir_for_json = os.path.join(test_data_dir, 'tmp_for_json_extraction')

        with zipfile.ZipFile(os.path.join('testdata', '20230206_vs_20230519',
                                          '20230519_focal_usn_data.zip'), 'r') as zip_ref:
            zip_ref.extractall(self.temp_dir_for_json)

    def test_manifest_and_json_matches_oscap(self):
        test_folder = os.path.join('testdata', '20230206_vs_20230519')

        manifest = ManifestFromJSONFile(os.path.join(test_folder, 'api_packagemanifest.json'))
        updates = PackagesUpdatesFromJSONFile(os.path.join(test_folder, 'api_packages_updates.json'))

        usn_data = USNDataFromManifestAndUpdatesJson(manifest=manifest, updates=updates, release="focal",
                                                     local_json_file=os.path.join(self.temp_dir_for_json,
                                                                                  "20230519_focal_usn_data.json"))

        # The expected vulnerabilities are the final two columns of oscap_report.html in the test folder
        expected_vulnerabilities = [
            (['[USN-5921-1]', '[CVE-2022-29154]'], 'USN-5921-1 -- rsync vulnerabilities'),
            (['[USN-5928-1]', '[CVE-2022-3821]', '[CVE-2022-4415]', '[CVE-2022-45873]'],
             'USN-5928-1 -- systemd vulnerabilities'),
            (['[USN-5936-1]', '[CVE-2022-3437]', '[CVE-2022-37966]', '[CVE-2022-37967]', '[CVE-2022-38023]',
              '[CVE-2022-42898]', '[CVE-2022-45141]'], 'USN-5936-1 -- Samba vulnerabilities'),
            (['[USN-5959-1]', '[CVE-2021-36222]', '[CVE-2021-37750]'], 'USN-5959-1 -- Kerberos vulnerabilities'),
            (['[USN-5960-1]', '[CVE-2023-24329]'], 'USN-5960-1 -- Python vulnerability'),
            (['[USN-5963-1]', '[CVE-2022-47024]', '[CVE-2023-0049]', '[CVE-2023-0051]', '[CVE-2023-0054]',
              '[CVE-2023-0288]', '[CVE-2023-0433]', '[CVE-2023-1170]', '[CVE-2023-1175]', '[CVE-2023-1264]'],
             'USN-5963-1 -- Vim vulnerabilities'),
            (['[USN-5964-1]', '[CVE-2023-27533]', '[CVE-2023-27534]', '[CVE-2023-27535]', '[CVE-2023-27536]',
              '[CVE-2023-27538]'], 'USN-5964-1 -- curl vulnerabilities'),
            # Add in the below, even though it is not shown on the OSCAP report, pending discussion
            # linux-cloud-tools-common and linux-tools-common is a binary from the linux source package, so
            # is showing up Linux kernel vulnerabilities even though this instance is running a later kernel
            # (which is what is tested in the OVAL file.
            (['[USN-5980-1]', '[CVE-2021-3669]', '[CVE-2022-2196]', '[CVE-2022-4382]', '[CVE-2023-23559]'],
             'USN-5980-1 -- Linux kernel vulnerabilities'),
            #
            (['[USN-5982-1]', '[CVE-2022-2196]', '[CVE-2022-3424]', '[CVE-2022-36280]', '[CVE-2022-41218]',
              '[CVE-2022-4382]', '[CVE-2022-48423]', '[CVE-2022-48424]', '[CVE-2023-0045]', '[CVE-2023-0210]',
              '[CVE-2023-0266]', '[CVE-2023-23454]', '[CVE-2023-23455]', '[CVE-2023-23559]', '[CVE-2023-26606]',
              '[CVE-2023-28328]'], 'USN-5982-1 -- Linux kernel vulnerabilities'),
            (['[USN-5993-1]', '[CVE-2023-0614]', '[CVE-2023-0922]'], 'USN-5993-1 -- Samba vulnerabilities'),
            (['[USN-5995-1]', '[CVE-2022-0413]', '[CVE-2022-1629]', '[CVE-2022-1674]', '[CVE-2022-1720]',
              '[CVE-2022-1733]', '[CVE-2022-1735]', '[CVE-2022-1785]', '[CVE-2022-1796]', '[CVE-2022-1851]',
              '[CVE-2022-1898]', '[CVE-2022-1927]', '[CVE-2022-1942]', '[CVE-2022-1968]', '[CVE-2022-2124]',
              '[CVE-2022-2125]', '[CVE-2022-2126]', '[CVE-2022-2129]', '[CVE-2022-2175]', '[CVE-2022-2183]',
              '[CVE-2022-2206]', '[CVE-2022-2304]', '[CVE-2022-2344]', '[CVE-2022-2345]', '[CVE-2022-2571]',
              '[CVE-2022-2581]', '[CVE-2022-2845]', '[CVE-2022-2849]', '[CVE-2022-2923]', '[CVE-2022-2946]',
              '[CVE-2022-2980]'], 'USN-5995-1 -- Vim vulnerabilities'),
            (['[USN-6005-1]', '[CVE-2023-28486]', '[CVE-2023-28487]'], 'USN-6005-1 -- Sudo vulnerabilities'),
            (['[USN-6018-1]', '[CVE-2023-1326]'], 'USN-6018-1 -- Apport vulnerability'),
            (['[USN-6025-1]', '[CVE-2022-4129]', '[CVE-2022-47929]', '[CVE-2022-4842]', '[CVE-2023-0386]',
              '[CVE-2023-0394]', '[CVE-2023-1073]', '[CVE-2023-1074]', '[CVE-2023-1281]', '[CVE-2023-1652]',
              '[CVE-2023-26545]'], 'USN-6025-1 -- Linux kernel vulnerabilities'),
            (['[USN-6026-1]', '[CVE-2021-4166]', '[CVE-2021-4192]', '[CVE-2021-4193]', '[CVE-2022-0213]',
              '[CVE-2022-0261]', '[CVE-2022-0318]', '[CVE-2022-0319]', '[CVE-2022-0351]', '[CVE-2022-0359]',
              '[CVE-2022-0361]', '[CVE-2022-0368]', '[CVE-2022-0408]', '[CVE-2022-0443]', '[CVE-2022-0554]',
              '[CVE-2022-0572]', '[CVE-2022-0629]', '[CVE-2022-0685]', '[CVE-2022-0714]', '[CVE-2022-0729]',
              '[CVE-2022-2207]'], 'USN-6026-1 -- Vim vulnerabilities'),
            # As above, added even though it is not in the oscap report.
            (['[USN-6027-1]', '[CVE-2022-3108]', '[CVE-2022-3903]', '[CVE-2022-4129]', '[CVE-2023-1073]',
              '[CVE-2023-1074]', '[CVE-2023-1281]', '[CVE-2023-26545]'], 'USN-6027-1 -- Linux kernel vulnerabilities'),
            #
            (['[USN-6028-1]', '[CVE-2023-28484]', '[CVE-2023-29469]'], 'USN-6028-1 -- libxml2 vulnerabilities'),
            (['[USN-6039-1]', '[CVE-2022-3996]', '[CVE-2023-0464]', '[CVE-2023-0466]'],
             'USN-6039-1 -- OpenSSL vulnerabilities'),
            (['[USN-6042-1]', '[CVE-2023-1786]'], 'USN-6042-1 -- Cloud-init vulnerability'),
            # As above, added even though not in the oscap report.
            (['[USN-6047-1]', '[CVE-2023-1829]'], 'USN-6047-1 -- Linux kernel vulnerability'),
            #
            (['[USN-6050-1]', '[CVE-2023-25652]', '[CVE-2023-25815]', '[CVE-2023-29007]'],
             'USN-6050-1 -- Git vulnerabilities'),
            (['[USN-6051-1]', '[CVE-2023-1829]', '[CVE-2023-1872]'], 'USN-6051-1 -- Linux kernel vulnerabilities'),
            (['[USN-6062-1]', '[CVE-2023-2004]'], 'USN-6062-1 -- FreeType vulnerability'),
            (['[USN-6080-1]', '[CVE-2022-27672]', '[CVE-2022-3707]', '[CVE-2023-0459]', '[CVE-2023-1075]',
              '[CVE-2023-1078]', '[CVE-2023-1118]', '[CVE-2023-1513]', '[CVE-2023-20938]', '[CVE-2023-2162]',
              '[CVE-2023-32269]'], 'USN-6080-1 -- Linux kernel vulnerabilities')]

        actual_vulnerabilities = usn_data.usns_and_cves_in_oscap_results_html_format()
        self.assertEqual(expected_vulnerabilities, actual_vulnerabilities)

    def test_update_vuln_table_summary_matches_oscap(self):
        test_folder = os.path.join('testdata', '20230206_vs_20230519')

        manifest = ManifestFromJSONFile(os.path.join(test_folder, 'api_packagemanifest.json'))
        updates = PackagesUpdatesFromJSONFile(os.path.join(test_folder, 'api_packages_updates.json'))

        usn_data = USNDataFromManifestAndUpdatesJson(manifest=manifest, updates=updates, release="focal",
                                                     local_json_file=os.path.join(self.temp_dir_for_json,
                                                                                  "20230519_focal_usn_data.json"))

        actual_vulnerabilities = \
            usn_data.tabulate_table_with_update_and_vulnerability_details()

        # The expected vulnerabilities are the final two columns of oscap_report.html in the test folder
        with open(os.path.join(test_folder, "updates_vulnerabilities.json")) as file:
            expected_vulnerabilities = json.load(file)
            self.assertEqual(expected_vulnerabilities, actual_vulnerabilities)

    def tearDown(self):
        shutil.rmtree(self.temp_dir_for_json)


class TestComparisonWithOscapMinimisedData(unittest.TestCase):

    def test_manifest_and_json_matches_oscap(self):
        test_folder = os.path.join('testdata', '20230206_vs_20230519')

        manifest = ManifestFromJSONFile(os.path.join(test_folder, 'api_packagemanifest.json'))
        updates = PackagesUpdatesFromJSONFile(os.path.join(test_folder, 'api_packages_updates.json'))

        usn_data = USNDataFromManifestAndUpdatesJson(
            manifest=manifest, updates=updates, release="focal",
            local_json_file=os.path.join('testdata', '20230525_full_usn_data_minimised.json'))

        # The expected vulnerabilities are the final two columns of oscap_report.html in the test folder
        expected_vulnerabilities = [
            (['[USN-5921-1]', '[CVE-2022-29154]'], 'USN-5921-1 -- rsync vulnerabilities'),
            (['[USN-5928-1]', '[CVE-2022-3821]', '[CVE-2022-4415]', '[CVE-2022-45873]'],
             'USN-5928-1 -- systemd vulnerabilities'),
            (['[USN-5936-1]', '[CVE-2022-3437]', '[CVE-2022-37966]', '[CVE-2022-37967]', '[CVE-2022-38023]',
              '[CVE-2022-42898]', '[CVE-2022-45141]'], 'USN-5936-1 -- Samba vulnerabilities'),
            (['[USN-5959-1]', '[CVE-2021-36222]', '[CVE-2021-37750]'], 'USN-5959-1 -- Kerberos vulnerabilities'),
            (['[USN-5960-1]', '[CVE-2023-24329]'], 'USN-5960-1 -- Python vulnerability'),
            (['[USN-5963-1]', '[CVE-2022-47024]', '[CVE-2023-0049]', '[CVE-2023-0051]', '[CVE-2023-0054]',
              '[CVE-2023-0288]', '[CVE-2023-0433]', '[CVE-2023-1170]', '[CVE-2023-1175]', '[CVE-2023-1264]'],
             'USN-5963-1 -- Vim vulnerabilities'),
            (['[USN-5964-1]', '[CVE-2023-27533]', '[CVE-2023-27534]', '[CVE-2023-27535]', '[CVE-2023-27536]',
              '[CVE-2023-27538]'], 'USN-5964-1 -- curl vulnerabilities'),
            # Add in the below, even though it is not shown on the OSCAP report, pending discussion
            # linux-cloud-tools-common and linux-tools-common is a binary from the linux source package, so
            # is showing up Linux kernel vulnerabilities even though this instance is running a later kernel
            # (which is what is tested in the OVAL file.
            (['[USN-5980-1]', '[CVE-2021-3669]', '[CVE-2022-2196]', '[CVE-2022-4382]', '[CVE-2023-23559]'],
             'USN-5980-1 -- Linux kernel vulnerabilities'),
            #
            (['[USN-5982-1]', '[CVE-2022-2196]', '[CVE-2022-3424]', '[CVE-2022-36280]', '[CVE-2022-41218]',
              '[CVE-2022-4382]', '[CVE-2022-48423]', '[CVE-2022-48424]', '[CVE-2023-0045]', '[CVE-2023-0210]',
              '[CVE-2023-0266]', '[CVE-2023-23454]', '[CVE-2023-23455]', '[CVE-2023-23559]', '[CVE-2023-26606]',
              '[CVE-2023-28328]'], 'USN-5982-1 -- Linux kernel vulnerabilities'),
            (['[USN-5993-1]', '[CVE-2023-0614]', '[CVE-2023-0922]'], 'USN-5993-1 -- Samba vulnerabilities'),
            (['[USN-5995-1]', '[CVE-2022-0413]', '[CVE-2022-1629]', '[CVE-2022-1674]', '[CVE-2022-1720]',
              '[CVE-2022-1733]', '[CVE-2022-1735]', '[CVE-2022-1785]', '[CVE-2022-1796]', '[CVE-2022-1851]',
              '[CVE-2022-1898]', '[CVE-2022-1927]', '[CVE-2022-1942]', '[CVE-2022-1968]', '[CVE-2022-2124]',
              '[CVE-2022-2125]', '[CVE-2022-2126]', '[CVE-2022-2129]', '[CVE-2022-2175]', '[CVE-2022-2183]',
              '[CVE-2022-2206]', '[CVE-2022-2304]', '[CVE-2022-2344]', '[CVE-2022-2345]', '[CVE-2022-2571]',
              '[CVE-2022-2581]', '[CVE-2022-2845]', '[CVE-2022-2849]', '[CVE-2022-2923]', '[CVE-2022-2946]',
              '[CVE-2022-2980]'], 'USN-5995-1 -- Vim vulnerabilities'),
            (['[USN-6005-1]', '[CVE-2023-28486]', '[CVE-2023-28487]'], 'USN-6005-1 -- Sudo vulnerabilities'),
            (['[USN-6018-1]', '[CVE-2023-1326]'], 'USN-6018-1 -- Apport vulnerability'),
            (['[USN-6025-1]', '[CVE-2022-4129]', '[CVE-2022-47929]', '[CVE-2022-4842]', '[CVE-2023-0386]',
              '[CVE-2023-0394]', '[CVE-2023-1073]', '[CVE-2023-1074]', '[CVE-2023-1281]', '[CVE-2023-1652]',
              '[CVE-2023-26545]'], 'USN-6025-1 -- Linux kernel vulnerabilities'),
            (['[USN-6026-1]', '[CVE-2021-4166]', '[CVE-2021-4192]', '[CVE-2021-4193]', '[CVE-2022-0213]',
              '[CVE-2022-0261]', '[CVE-2022-0318]', '[CVE-2022-0319]', '[CVE-2022-0351]', '[CVE-2022-0359]',
              '[CVE-2022-0361]', '[CVE-2022-0368]', '[CVE-2022-0408]', '[CVE-2022-0443]', '[CVE-2022-0554]',
              '[CVE-2022-0572]', '[CVE-2022-0629]', '[CVE-2022-0685]', '[CVE-2022-0714]', '[CVE-2022-0729]',
              '[CVE-2022-2207]'], 'USN-6026-1 -- Vim vulnerabilities'),
            # As above, added even though it is not in the oscap report.
            (['[USN-6027-1]', '[CVE-2022-3108]', '[CVE-2022-3903]', '[CVE-2022-4129]', '[CVE-2023-1073]',
              '[CVE-2023-1074]', '[CVE-2023-1281]', '[CVE-2023-26545]'], 'USN-6027-1 -- Linux kernel vulnerabilities'),
            #
            (['[USN-6028-1]', '[CVE-2023-28484]', '[CVE-2023-29469]'], 'USN-6028-1 -- libxml2 vulnerabilities'),
            (['[USN-6039-1]', '[CVE-2022-3996]', '[CVE-2023-0464]', '[CVE-2023-0466]'],
             'USN-6039-1 -- OpenSSL vulnerabilities'),
            (['[USN-6042-1]', '[CVE-2023-1786]'], 'USN-6042-1 -- Cloud-init vulnerability'),
            # As above, added even though not in the oscap report.
            (['[USN-6047-1]', '[CVE-2023-1829]'], 'USN-6047-1 -- Linux kernel vulnerability'),
            #
            (['[USN-6050-1]', '[CVE-2023-25652]', '[CVE-2023-25815]', '[CVE-2023-29007]'],
             'USN-6050-1 -- Git vulnerabilities'),
            (['[USN-6051-1]', '[CVE-2023-1829]', '[CVE-2023-1872]'], 'USN-6051-1 -- Linux kernel vulnerabilities'),
            (['[USN-6062-1]', '[CVE-2023-2004]'], 'USN-6062-1 -- FreeType vulnerability'),
            (['[USN-6080-1]', '[CVE-2022-27672]', '[CVE-2022-3707]', '[CVE-2023-0459]', '[CVE-2023-1075]',
              '[CVE-2023-1078]', '[CVE-2023-1118]', '[CVE-2023-1513]', '[CVE-2023-20938]', '[CVE-2023-2162]',
              '[CVE-2023-32269]'], 'USN-6080-1 -- Linux kernel vulnerabilities')]

        actual_vulnerabilities = usn_data.usns_and_cves_in_oscap_results_html_format()
        self.assertEqual(expected_vulnerabilities, actual_vulnerabilities)

    def test_update_vuln_table_summary_matches_oscap(self):
        test_folder = os.path.join('testdata', '20230206_vs_20230519')

        manifest = ManifestFromJSONFile(os.path.join(test_folder, 'api_packagemanifest.json'))
        updates = PackagesUpdatesFromJSONFile(os.path.join(test_folder, 'api_packages_updates.json'))

        usn_data = USNDataFromManifestAndUpdatesJson(
            manifest=manifest, updates=updates, release="focal",
            local_json_file=os.path.join('testdata', '20230525_full_usn_data_minimised.json'))

        actual_vulnerabilities = \
            usn_data.tabulate_table_with_update_and_vulnerability_details()

        # The expected vulnerabilities are the final two columns of oscap_report.html in the test folder
        with open(os.path.join(test_folder, "updates_vulnerabilities.json")) as file:
            expected_vulnerabilities = json.load(file)
            self.assertEqual(expected_vulnerabilities, actual_vulnerabilities)


class TestComplexManifestAndUpdates(unittest.TestCase):

    def test_complex_manifest_and_updates_file(self):
        test_folder = os.path.join('testdata')

        manifest = ManifestFromJSONFile(os.path.join(test_folder, 'complex_jammy_manifest.json'))
        updates = PackagesUpdatesFromJSONFile(os.path.join(test_folder, 'complex_jammy_available_updates.json'))

        usn_data = USNDataFromManifestAndUpdatesJson(
            manifest=manifest, updates=updates, release="focal",
            local_json_file=os.path.join('testdata', '20230525_full_usn_data_minimised.json'))


if __name__ == '__main__':
    unittest.main()

