import json
import os
import unittest
from main import ManifestFromJSONFile


class TestManifest(unittest.TestCase):

    def test_manifest_json_to_dictionary(self):
        manifest = ManifestFromJSONFile(os.path.join('testdata', '20230206_vs_20230519', 'api_packagemanifest.json'))

        with open(os.path.join('testdata', '20230206_vs_20230519', 'manifest_debs.json'), 'r') as file:
            expected_dictionary = json.load(file)
            self.assertEqual(expected_dictionary, manifest.deb_list)

        with open(os.path.join('testdata', '20230206_vs_20230519', 'manifest_snaps.json'), 'r') as file:
            expected_dictionary = json.load(file)
            self.assertEqual(expected_dictionary, manifest.snap_list)


if __name__ == '__main__':
    unittest.main()

