## Goal
This project is a proof-of-concept testing out approaches to use the [Ubuntu Security API](https://ubuntu.com/security/api/docs) and [Ubuntu Pro Client API](https://canonical-ubuntu-pro-client.readthedocs-hosted.com/en/latest/references/api.html) to make security assessments on Ubuntu instances.

Canonical recommends the use of [OVAL and the oscap tool](https://ubuntu.com/security/oval) to assess the security posture of an individual instance.

This project explores approaches to extract information from instances and analyse them separately, likely on a control plane for multiple instances.

## Requirements

This tool requires:
- Python 3 and the following Python packages:
- requests
- python-debian
- tabulate

## Try it out

### 1. Test with demo data
We want to use an Ubuntu instance for this test. I will launch a fresh Ubuntu 22.04 (Jammy Jellyfish) container in LXD to do this, but this is not required:
```commandline
lxc launch ubuntu:22.04 security-tools-test
lxc exec security-tools-test bash
```
We clone the repository:
```
git clone https://gitlab.com/aaron-w/ubuntu-python-security-tools.git
cd ubuntu-python-security-tools/
```
Install dependencies:
```commandline
apt update
apt install python3-pip
pip install requests python-debian tabulate
```

Run this with test data:
```commandline
python3 main.py tests/testdata/20230206_vs_20230519/api_packagemanifest.json tests/testdata/20230206_vs_20230519/api_packages_updates.json focal --local_USN_data=tests/testdata/20230525_full_usn_data_minimised.json | more
```

Now we see something like this:
![test_data_screenshot.png](test_data_screenshot.png)

### 2. Test with updated USN data
The above ran on data that is shipped as part of the repository. For a more realistic demonstration, we can run the same test, but using up-to-date USN data. To do this, we simply omit the `--local_USN_data=` from the end of the command.

```commandline
python3 main.py tests/testdata/20230206_vs_20230519/api_packagemanifest.json tests/testdata/20230206_vs_20230519/api_packages_updates.json focal
```
We see that it starts to download data. The first run (and every 24 hours) it will need to download the data from the Security API, which even on a good connection will likely take 5-10 minutes. It will also need a few GB of free RAM. This is because it is downloading and processing all the USN data from the Security API, which is about 2.3GB at the time of writing.
```commandline
No local JSON file cached, downloading.
response = requests.get(https://ubuntu.com/security/notices.json, params={'limit': 10, 'offset': 0})
Requesting USN records 11 to 20 (of 7486)
Requesting USN records 21 to 30 (of 7486)
Requesting USN records 31 to 40 (of 7486)
Requesting USN records 41 to 50 (of 7486)
Requesting USN records 51 to 60 (of 7486)
Requesting USN records 61 to 70 (of 7486)
[...]
```
After the first run it will create a minimised version of the USN JSON data from the API, removing everything except the data used for current features, and will use this until the data needs to be refreshed. This minimised JSON is only 23MB, so a significant reduction in size, and by default is stored at `.cache/usn_data_minimised.json`. As we saw in the first example, even though this data is 100 times smaller, it has all the data we need to analyse a system and can be used instead of downloading the data, for example we copied this minimised file to another system.

Even though this is downloading fresh USN data, the output should be the same as in the earlier step, as we are using the same manifest file and file containing the available updates. Those files are a snapshot of what was installed on the instance and what updates it could see available in the repositories at the time those files were created. Even if new USNs are released after creating those files, they are unlikely to change the vulnerabilities that were fixed by the updates available at the time that snapshot was taken.

### 3. Test with real data
We will now test with our own data, which is always more unpredictable!

To make things interesting, lets collect information from our host system and analyse it in our container.

On our **host** instance (or any other Ubuntu instance) first let's check that the Ubuntu Pro client is up-to-date:
```commandline
sudo apt update
sudo apt install ubuntu-advantage-tools
```
Then we can collect information about the installed packages using the [Ubuntu Pro Client API](https://canonical-ubuntu-pro-client.readthedocs-hosted.com/en/latest/references/api.html):
```commandline
pro api u.security.package_manifest.v1 > host_manifest.json
```
And information on the updates that it can see:
```commandline
pro api u.pro.packages.updates.v1 > host_available_updates.json
```
Finally, we need to check what release the host instance is running, which you will need to insert into the command further down as your `[release]`:
```commandline
lsb_release -c -s
```
Then let's push those files (or copy them) to our container:
```commandline
lxc file push host_manifest.json security-tools-test/root/ubuntu-python-security-tools/
lxc file push host_available_updates.json security-tools-test/root/ubuntu-python-security-tools/
```
And now let's analyse those **in our container**, so we go back to where we were running things before and run:
```commandline
python3 main.py host_manifest.json host_available_updates.json [release]
```
In my case, that is:
```commandline
python3 main.py host_manifest.json host_available_updates.json jammy
```
This may or may not show you anything interesting, depending on whether you have available updates and those updates have security vulnerabilities. On my machine, I have regular updates but security updates are installed automatically by `unattended-upgrades`, so no vulnerabilities are shown.

## A note on performance and code quality
This project is currently just a proof of concept. There are many areas where performance could be improved and many areas where code could be written far more elegantly. 